package S1;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class DataService {

    @NotNull
    static List<Data> sameName(@NotNull List<Data> dataList, String name) {
        List<Data> res = new LinkedList<>();
        for (Data idx : dataList) {
            if (idx.getName().equals(name)) {
                res.add(idx);
            }
        }
        return res;
    }

    @NotNull
    static List<Data> sameValue(@NotNull List<Data> dataList, double level) {
        List<Data> res = new LinkedList<>();
        for (Data idx : dataList) {
            if (Double.compare(idx.getValue(), level) <= 0) {
                res.add(idx);
            }
        }
        return res;
    }

    @NotNull
    static Set<Double> ListItemsNamesThatHaveManyNames(@NotNull List<Data> list, Set<String> set) {
        Set<Double> res = new HashSet<>();
        for (Data idx : list) {
            if (set.contains(idx.getName())) {
                res.add(idx.getValue());
            }
        }
        return res;
    }

    @NotNull
    static String[] elementsForWhichTheValueOfTheValueFieldPositive(@NotNull List<Data> list) {
        SortedSet<String> strings = new TreeSet<>();
        for (Data idx : list) {
            if (Double.compare(idx.getValue(), 0) > 0) {
                strings.add(idx.getName());
            }
        }
        List<String> preRes = new ArrayList<>(strings);
        return (String[]) preRes.toArray();
    }
}
